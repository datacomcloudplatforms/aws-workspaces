AWSTemplateFormatVersion: '2010-09-09'
Description: 'AWS Ops Template for AWS Workspaces'

Parameters:
  EnvironmentFullName:
    Description: The FQDN Name of the Simple AD Instance Supporting Workspaces
    Type: String
  EnvironmentShortName:
    Description: The NetBIOS Name of the Simple AD Instance Supporting Workspaces
    Type: String
  EnvironmentDescription:
    Description: Description for the Directory Services Instance
    Type: String
  VpcCIDR:
    Description: New VPC Networking Range for Workspaces Environment
    Type: String
  AZ1PublicSubnetCIDR:
    Description: Public Subnet for Workspaces Environment in AZ1
    Type: String
  AZ2PublicSubnetCIDR:
    Description: Public Subnet for Workspaces Environment in AZ2
    Type: String
  AZ1PrivateSubnetCIDR:
    Description: Private Subnet in the first AZ for Workspaces Environment
    Type: String
  AZ2PrivateSubnetCIDR:
    Description: Private Subnet in the second AZ for Workspaces Environment
    Type: String
  AZ1TGWSubnetCIDR:
    Description: Private Subnet in the first AZ for Workspaces TGW
    Type: String
  AZ2TGWSubnetCIDR:
    Description: Private Subnet in the second AZ for Workspaces TGW
    Type: String
  AZ1NFWSubnetCIDR:
    Description: Private Subnet in the first AZ for AWS Network Firewall
    Type: String
  AZ2NFWSubnetCIDR:
    Description: Private Subnet in the second AZ for AWS Network Firewall
    Type: String
  TechnicalOwner:
    Description: Environment Description and Referance Tag for Infrastructure
    Type: String
  BusinessOwner:
    Description: Environment Description and Referance Tag for Infrastructure
    Type: String
  TransitGatewayID:
    Description: TransitGatewayID - copied and pasted in manually after TGW created as there's no way to transfer the information between accounts
    Type: String
  NetworkFirewallID:
    Description: NetworkFirewallID - see test notes
    Type: String

  #### Option Groups ###
  EnableDiagnosticFlowLogs:
    Description: VPC flow log state
    Type: String
    Default: disabled
    AllowedValues: [enabled, disabled]
  #### Set this parameter's default value to enable or disable flow logs ###
  TransitGatewayPresent:
    Description: Transit Gateway Usage
    Type: String
    Default: disabled
    AllowedValues: [enabled, disabled]
  #### Set this parameter to define if a TGW is available or not ###
  NewtorkFirewallPresent:
    Description: Network FIrewall Usage
    Type: String
    Default: disabled
    AllowedValues: [enabled, disabled]
  #### Set this parameter to define if a NFW is available or not ###

Conditions: 
  DiagnosticFlowLogsEnable: !Equals [ !Ref EnableDiagnosticFlowLogs, "enabled"]
  TransitGatewayAvailable: !Equals [ !Ref TransitGatewayPresent, "enabled"]
  NetworkFirewallAvailable: !Equals [ !Ref NewtorkFirewallPresent, "enabled"]

Resources:
  # Starting the Creation of the VPC to Support Workspaces
  WorkspacesVPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VpcCIDR
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - Key: Name
          Value: !Ref EnvironmentShortName
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  PublicSubnetA:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref WorkspacesVPC
      AvailabilityZone: !Select [ 0, !GetAZs '' ]
      CidrBlock: !Ref AZ1PublicSubnetCIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} Public Subnet AZ1
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  PublicSubnetB:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref WorkspacesVPC
      AvailabilityZone: !Select [ 1, !GetAZs '' ]
      CidrBlock: !Ref AZ2PublicSubnetCIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} Public Subnet AZ2
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  PrivateSubnetA:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref WorkspacesVPC
      AvailabilityZone: !Select [ 0, !GetAZs  '' ]
      CidrBlock: !Ref AZ1PrivateSubnetCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} Private Subnet AZ1
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner
    
  PrivateSubnetB:
    Type: AWS::EC2::Subnet
    Properties:
      VpcId: !Ref WorkspacesVPC
      AvailabilityZone: !Select [ 1, !GetAZs  '' ]
      CidrBlock: !Ref AZ2PrivateSubnetCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} Private Subnet AZ2
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  TGWSubnetA:
    Type: AWS::EC2::Subnet
    Condition: TransitGatewayAvailable
    Properties:
      VpcId: !Ref WorkspacesVPC
      AvailabilityZone: !Select [ 0, !GetAZs  '' ]
      CidrBlock: !Ref AZ1TGWSubnetCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} TGW Subnet AZ1
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  TGWSubnetB:
    Type: AWS::EC2::Subnet
    Condition: TransitGatewayAvailable
    Properties:
      VpcId: !Ref WorkspacesVPC
      AvailabilityZone: !Select [ 1, !GetAZs  '' ]
      CidrBlock: !Ref AZ2TGWSubnetCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} TGW Subnet AZ2
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  NFWSubnetA:
    Type: AWS::EC2::Subnet
    Condition: NetworkFirewallAvailable
    Properties:
      VpcId: !Ref WorkspacesVPC
      AvailabilityZone: !Select [ 0, !GetAZs  '' ]
      CidrBlock: !Ref AZ1NFWSubnetCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} NFW Subnet AZ1
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  NFWSubnetB:
    Type: AWS::EC2::Subnet
    Condition: NetworkFirewallAvailable
    Properties:
      VpcId: !Ref WorkspacesVPC
      AvailabilityZone: !Select [ 1, !GetAZs  '' ]
      CidrBlock: !Ref AZ2NFWSubnetCIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} NFW Subnet AZ2
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} Public Routes
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  PublicSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnetA

  PublicSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnetB

  PrivateRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} Private Routes
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  PrivateSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnetA

  PrivateSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnetB

  TGWRouteTable:
    Type: AWS::EC2::RouteTable
    Condition: TransitGatewayAvailable
    Properties:
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} TGW Route
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  TGWSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Condition: TransitGatewayAvailable
    Properties:
      RouteTableId: !Ref TGWRouteTable
      SubnetId: !Ref TGWSubnetA

  TGWSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Condition: TransitGatewayAvailable
    Properties:
      RouteTableId: !Ref TGWRouteTable
      SubnetId: !Ref TGWSubnetB

  NFWRouteTable:
    Type: AWS::EC2::RouteTable
    Condition: NetworkFirewallAvailable
    Properties:
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} NFW Route
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  NFWSubnet1RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Condition: NetworkFirewallAvailable
    Properties:
      RouteTableId: !Ref NFWRouteTable
      SubnetId: !Ref NFWSubnetA

  NFWSubnet2RouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Condition: NetworkFirewallAvailable
    Properties:
      RouteTableId: !Ref NFWRouteTable
      SubnetId: !Ref NFWSubnetB

  # TGW NACL
  TGWNetworkAcl:
    Type: AWS::EC2::NetworkAcl
    Condition: TransitGatewayAvailable
    Properties:
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} NACL TGW
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  NFWNetworkAcl:
    Type: AWS::EC2::NetworkAcl
    Condition: NetworkFirewallAvailable
    Properties:
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} NACL NFW
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  # Associate TGW NACL with subnets
  NaclSubnetAssociationTGWA:
    Type: AWS::EC2::SubnetNetworkAclAssociation
    Condition: TransitGatewayAvailable
    Properties: 
      NetworkAclId: !Ref TGWNetworkAcl
      SubnetId: !Ref TGWSubnetA
  NaclSubnetAssociationTGWB:
    Type: AWS::EC2::SubnetNetworkAclAssociation
    Condition: TransitGatewayAvailable
    Properties: 
      NetworkAclId: !Ref TGWNetworkAcl
      SubnetId: !Ref TGWSubnetB

  TGWNACLOutbound:
    Type: AWS::EC2::NetworkAclEntry
    Condition: TransitGatewayAvailable
    Properties:
      NetworkAclId: !Ref TGWNetworkAcl
      RuleNumber: 100
      Protocol: -1 # All
      RuleAction: allow
      Egress: true
      CidrBlock: 0.0.0.0/0
  TGWNACLReturn:
    Type: AWS::EC2::NetworkAclEntry
    Condition: TransitGatewayAvailable
    Properties:
      NetworkAclId: !Ref TGWNetworkAcl
      RuleNumber: 100
      Protocol: -1 # All
      RuleAction: allow
      Egress: false
      CidrBlock: 0.0.0.0/0

  # Associate NFW NACL with subnets
  NaclSubnetAssociationNFWA:
    Type: AWS::EC2::SubnetNetworkAclAssociation
    Condition: NetworkFirewallAvailable
    Properties: 
      NetworkAclId: !Ref NFWNetworkAcl
      SubnetId: !Ref NFWSubnetA
  NaclSubnetAssociationNFWB:
    Type: AWS::EC2::SubnetNetworkAclAssociation
    Condition: NetworkFirewallAvailable
    Properties: 
      NetworkAclId: !Ref NFWNetworkAcl
      SubnetId: !Ref NFWSubnetB

  NFWNACLOutbound:
    Type: AWS::EC2::NetworkAclEntry
    Condition: NetworkFirewallAvailable
    Properties:
      NetworkAclId: !Ref NFWNetworkAcl
      RuleNumber: 100
      Protocol: -1 # All
      RuleAction: allow
      Egress: true
      CidrBlock: 0.0.0.0/0
  NFWNACLReturn:
    Type: AWS::EC2::NetworkAclEntry
    Condition: NetworkFirewallAvailable
    Properties:
      NetworkAclId: !Ref NFWNetworkAcl
      RuleNumber: 100
      Protocol: -1 # All
      RuleAction: allow
      Egress: false
      CidrBlock: 0.0.0.0/0

  # Public NACL allows traffic to / from private subnet and https internet access
  WorkspacesNetworkAclPublic:
    Type: AWS::EC2::NetworkAcl
    Properties:
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} NACL Public
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  # Associate NACL with subnets
  NaclSubnetAssociationPublicA:
    Type: AWS::EC2::SubnetNetworkAclAssociation
    Properties: 
      NetworkAclId: !Ref WorkspacesNetworkAclPublic
      SubnetId: !Ref PublicSubnetA
  NaclSubnetAssociationPublicB:
    Type: AWS::EC2::SubnetNetworkAclAssociation
    Properties: 
      NetworkAclId: !Ref WorkspacesNetworkAclPublic
      SubnetId: !Ref PublicSubnetB

  # Private NACLs allows communication with internet and environment CIDR ranges
  WorkspacesNetworkAclPrivate:
    Type: AWS::EC2::NetworkAcl
    Properties:
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} NACL Private
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  NaclSubnetAssociationPrivateA:
    Type: AWS::EC2::SubnetNetworkAclAssociation
    Properties: 
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      SubnetId: !Ref PrivateSubnetA
  NaclSubnetAssociationPrivateB:
    Type: AWS::EC2::SubnetNetworkAclAssociation
    Properties: 
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      SubnetId: !Ref PrivateSubnetB

  PrivateNACLIntraOnly:
    Type: AWS::EC2::NetworkAclEntry
    Properties:
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      RuleNumber: 100
      Protocol: -1 # All
      RuleAction: allow
      Egress: true
      CidrBlock: !Ref VpcCIDR
      PortRange:
        From: 0
        To: 65535
  
  PrivateNACLReturnIntraOnly:
    Type: AWS::EC2::NetworkAclEntry
    Properties:
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      RuleNumber: 100
      Protocol: -1 # All
      RuleAction: allow
      Egress: false
      CidrBlock: !Ref VpcCIDR
      PortRange:
        From: 0
        To: 65535

  PrivateNACLHTTPS:
    Type: AWS::EC2::NetworkAclEntry
    Properties:
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      RuleNumber: 110
      Protocol: 6 # TCP
      RuleAction: allow
      Egress: true
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: 443
        To: 443
  
  PrivateNACLReturnHTTPS:
    Type: AWS::EC2::NetworkAclEntry
    Properties:
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      RuleNumber: 110
      Protocol: 6 # TCP
      RuleAction: allow
      Egress: false
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: 49152
        To: 65535

  PrivateNACLRDPEgress:
    Type: AWS::EC2::NetworkAclEntry
    Properties:
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      RuleNumber: 130
      Protocol: 6 # TCP
      RuleAction: allow
      Egress: true
      CidrBlock: 10.136.32.0/21
      PortRange:
        From: 3389
        To: 3389
  
  PrivateNACLRDPIngress:
    Type: AWS::EC2::NetworkAclEntry
    Properties:
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      RuleNumber: 140
      Protocol: 6 # TCP
      RuleAction: allow
      Egress: false
      CidrBlock: 10.136.32.0/21
      PortRange:
        From: 49152
        To: 65535

  PrivateNACLSSHEgress:
    Type: AWS::EC2::NetworkAclEntry
    Properties:
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      RuleNumber: 150
      Protocol: 6 # TCP
      RuleAction: allow
      Egress: true
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: 22
        To: 22
  
  PrivateNACLSSHIngress:
    Type: AWS::EC2::NetworkAclEntry
    Properties:
      NetworkAclId: !Ref WorkspacesNetworkAclPrivate
      RuleNumber: 150
      Protocol: 6 # TCP
      RuleAction: allow
      Egress: false
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: 22
        To: 22

  # Start Creation of SimpleAD Service to Support Workspaces
  WorkspacesMasterKey:
    Type: AWS::SecretsManager::Secret
    Properties: 
      Name: "WORKSPACES/DIRECTORY-ADMIN"
      Description: Simple AD Admin Password for Workspaces
      GenerateSecretString:
        SecretStringTemplate: '{"password":"thisistemptext"}'
        GenerateStringKey: "password"
        IncludeSpace: False
        PasswordLength: 26
      Tags:
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

  WorkspacesDefaultRole:
    # This role is automatically created on Console/Manual Setup of WorkSpaces but MUST be scripted when using CFN
    Type: AWS::IAM::Role
    Properties: 
      RoleName: workspaces_DefaultRole
      Description: "Default Role for AWS Workspaces"
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - workspaces.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Path: /
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonWorkSpacesServiceAccess
        - arn:aws:iam::aws:policy/AmazonWorkSpacesSelfServiceAccess
      Tags: 
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner
          
  WorkspacesDirectory:
    Type: AWS::DirectoryService::SimpleAD
    Properties: 
      CreateAlias: False
      Description: !Ref EnvironmentDescription
      EnableSso: False
      Name: !Ref EnvironmentFullName
      Password: !Join ['', ['{{resolve:secretsmanager:', !Ref WorkspacesMasterKey, ':SecretString:password}}' ]]
      ShortName: !Ref EnvironmentShortName
      Size: Small
      VpcSettings: 
        SubnetIds: 
          - !Ref PrivateSubnetA
          - !Ref PrivateSubnetB
        VpcId: !Ref WorkspacesVPC


  # Private subnets need a route to the transit gateway to reach environments and NAT egress
  TransitGatewayPrivate1SubnetRoute:
    DependsOn: TGWAttachment
    Type: AWS::EC2::Route
    Condition: TransitGatewayAvailable
    Properties:
       RouteTableId: !Ref PrivateRouteTable
       DestinationCidrBlock: 0.0.0.0/0
       TransitGatewayId: !Ref TransitGatewayID

  TGWAttachment:
    Type: "AWS::EC2::TransitGatewayAttachment"
    Condition: TransitGatewayAvailable
    Properties:
      TransitGatewayId: !Ref TransitGatewayID
      SubnetIds:
        - !Ref TGWSubnetA
        - !Ref TGWSubnetB
      VpcId: !Ref WorkspacesVPC
      Tags:
        - Key: Name
          Value: !Sub ${EnvironmentShortName} TGW Attachment
        - Key: TechnicalOwner
          Value: !Ref TechnicalOwner
        - Key: BusinessOwner
          Value: !Ref BusinessOwner

# Create a flow log if the condition is enabled
  VPCFlowLogCompliance:
    Type: AWS::EC2::FlowLog
    Properties:
      DeliverLogsPermissionArn: !ImportValue CloudwatchLogsRoleARN
      LogDestinationType: cloud-watch-logs
      LogGroupName: !ImportValue VPCFlowLogGroupCompliance
      TrafficType: REJECT
      ResourceId: !Ref WorkspacesVPC
      ResourceType: VPC

  VPCFlowLogGroupDiagnostic:
    Type: AWS::EC2::FlowLog
    Condition: DiagnosticFlowLogsEnable
    Properties:
      DeliverLogsPermissionArn: !ImportValue CloudwatchLogsRoleARN
      LogDestinationType: cloud-watch-logs
      LogGroupName: !ImportValue VPCFlowLogGroupDiagnostic
      TrafficType: ALL
      ResourceId: !Ref WorkspacesVPC
      ResourceType: VPC

