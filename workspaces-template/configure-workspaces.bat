@ECHO OFF

REM This Batch Script Configures AWS WorkSpaces using AWS CLI Methods
REM There are multiple adaptable Options in the scrtipt, please see README.md

REM Set Deployment Parameters for Batch File Here
SET AWSProfileName=see-documentation

REM Get the new Directory ID from the Workspaces CloudFormation Stack
aws ds --profile %AWSProfileName% describe-directories --query "DirectoryDescriptions[*].DirectoryId" --no-paginate --output text > tempData
set /p DirectoryID= < tempData
del tempData

REM Register Workspaces Directory
aws workspaces --profile %AWSProfileName% register-workspace-directory --directory-id %DirectoryID% --no-enable-work-docs --no-enable-self-service

REM Configure Workspaces Directory
aws workspaces --profile %AWSProfileName% modify-selfservice-permissions --resource-id %DirectoryID% --selfservice-permissions ^
    RestartWorkspace=ENABLED,IncreaseVolumeSize=DISABLED,ChangeComputeType=DISABLED,SwitchRunningMode=DISABLED,RebuildWorkspace=ENABLED
aws workspaces --profile %AWSProfileName% modify-workspace-access-properties --resource-id %DirectoryID% --workspace-access-properties ^
    DeviceTypeWindows=ALLOW,DeviceTypeOsx=ALLOW,DeviceTypeWeb=DENY,DeviceTypeIos=DENY,DeviceTypeAndroid=DENY,DeviceTypeChromeOs=DENY,DeviceTypeZeroClient=DENY

REM Create IP Address Access Rules
aws workspaces --profile %AWSProfileName% create-ip-group --group-name "Datacom Office" --user-rules ^
    ipRule=202.27.76.248/31,ruleDesc=datacom-auckland-dcom-dg ^
    ipRule=202.27.76.201/32,ruleDesc=datacom-auckland-byo-vpn ^
    ipRule=202.27.76.101/32,ruleDesc=datacom-auckland-corp ^
    ipRule=202.175.141.80/31,ruleDesc=datacom-wellington-dcom ^
    ipRule=202.175.142.225/32,ruleDesc=datacom-wellington-byo ^
    ipRule=202.175.142.224/32,ruleDesc=datacom-wellington-corp

aws workspaces --profile %AWSProfileName% create-ip-group --group-name "Client Office" --user-rules ^
    ipRule=205.81.102.222/32,ruleDesc=location-one ^
    ipRule=205.81.102.223/32,ruleDesc=location-two ^
    ipRule=205.81.102.224/32,ruleDesc=location-three

aws workspaces --profile %AWSProfileName% create-ip-group --group-name "WFH Staff" --user-rules ^
    ipRule=209.81.102.222/32,ruleDesc=user1-staticip ^
    ipRule=209.81.102.223/32,ruleDesc=user2-staticip ^
    ipRule=209.81.102.224/32,ruleDesc=user3-staticip

REM OPTIONAL - Associate WorkSapces with DNS in another Account
@REM aws route53 --profile target-account create-vpc-association-authorization --hosted-zone-id Z0abcxyzHP --vpc VPCRegion=ap-southeast-2,VPCId=vpc-0abcxyz2
@REM aws route53 --profile %AWSProfileName% associate-vpc-with-hosted-zone --hosted-zone-id  Z0abcxyzHP --vpc VPCRegion=ap-southeast-2,VPCId=vpc-0abcxyz2

ECHO Workspaces Environment Configuration Script has Completed, Have a Nice Day :)