import boto3, time, csv
from io import StringIO
from botocore.exceptions import ClientError
from botocore.config import Config
from datetime import datetime, timezone

# This script is written for SAM / Lambda to detect WorkSpaces unused for x Days and Terminate them

ssm_parameter = boto3.client('ssm')
ALERT_DISABLE = 90
ALERT_DAYS = ALERT_DISABLE - 10
ALERT_MAX = ALERT_DISABLE - 8

sns_client = boto3.client('sns')
wks_client = boto3.client('workspaces')
ds_client = boto3.client('ds')

def workspaces_age():
    count = 0
    emailReport = "WorkSpace Idle Checks \n"
    wks_status = wks_client.describe_workspaces_connection_status()
    wks_list = list(wks_status.values())[0]

    for workspace in wks_list:
        WsReport = "Checking for unused WorkSpaces \n"
        age = datetime.now(timezone.utc) - workspace['LastKnownUserConnectionTimestamp']
        if age.days > ALERT_DISABLE:
            count += 1
            WsReport += "WARNING: WorkSpace" + workspace['WorkspaceId'] + "has not been used for" + age.days + "Days, Terminating this WorkSpace \n"
            print("WARNING: WorkSpace", workspace['WorkspaceId'], "has not been used for", age.days, "Days, Terminating this WorkSpace")
            wks_client.terminate_workspaces(
                TerminateWorkspaceRequests=[
                    {
                        'WorkspaceId': workspace['WorkspaceId']
                    },
                ]
            )
        elif age.days > ALERT_DAYS:
            count += 1
            WsReport += "WARNING: WorkSpace" + workspace['WorkspaceId'] + "has not been used for" + age.days + "Days and will be deleted after" + ALERT_DISABLE + "days of non-use \n"
            print("WARNING: WorkSpace", workspace['WorkspaceId'], "has not been used for", age.days, "Days and will be deleted after" + ALERT_DISABLE + "days of non-use")
    if count == 0:
        WsReport += " - No Compliance Issues found with the Age of these accounts \n"
        print(" - No Compliance Issues found with the Age of these accounts")
    return WsReport

def lambda_handler(event, context):
    workspaces_age()