# README #

This Repository covers the automated deployment of AWS WorkSpaces.

Coverage of this document includes,

* Creating a new VPC for isolating WorkSpaces (Recommended)
* Creates a SimpleAD Directory Service via YAML/CFN (Est. $50/month)
* Registers the Directory to WorkSpaces via AWS CLI
* Configures the WorkSpaces via AWS CLI
* Creates IP Groups and Security Filtering

This Reposotry DOES NOT Cover (But Guides Documented Below),

* Filtering Connection Security via Certificate
* WorkSpaces Imaging and Custom Templates
* Desktop Authentication with MFA

### Overview of the Solution ###

* Hello Datacom
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Using or Deploying this Solution ###

Document Process Here in Detail

### Beyond this Solution ###

Details to be documented for MFA, GPO and UbiKey Solutions

### Who do I talk to? ###

* Sorry, the Author no longer works for Datacom => https://github.com/GrumpyBum
* Solution driven by an internal Architect => Tim Wild, timwi@datacom.co.nz
* Brought together by Paul Dunlop, paul.dunlop@datacom.co.nz
* Refure to the Datacom CPS Team in Wellington for AWS Support around this solution

### Known Issues ###

* Datacom Exchange/Office 365 Services block AWS WorkSapces emails
* If an account password expires this can cause directory services issues
* OTKA is not Compatable with PCoIP WorkSpaces (C++ .NET Conflict)
* EC2 is a great resource for unlocaking the Directory Services